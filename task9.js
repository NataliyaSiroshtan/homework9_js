let tabNavs = document.querySelectorAll(".tabs-title");
let tabPanes = document.querySelectorAll(".tabs-content-item");
let nav = document.querySelector(".tabs");

nav.addEventListener("click", function (event) {
  for (let i = 0; i < tabNavs.length; i++) {
    if (tabNavs[i] === event.target) {
      event.target.classList.add("active");
    } else if (tabNavs[i] !== event.target) {
      tabNavs[i].classList.remove("active");
    }
  }
  let dataAttribute = document.querySelector(
    `[data-tab-content=${event.target.id}]`
  );
  for (let i = 0; i < tabPanes.length; i++) {
    if (dataAttribute === tabPanes[i]) {
      console.log(dataAttribute);
      dataAttribute.classList.remove("noActiveItem");
      dataAttribute.classList.add("activeItem");
    } else {
      tabPanes[i].classList.remove("activeItem");
      tabPanes[i].classList.add("noActiveItem");
    }
  }
});
